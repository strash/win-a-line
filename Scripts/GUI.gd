extends Control


signal button_back_pressed


var REGEX = RegEx.new()


var score: int = 0
var score_step: int = 50
var button_pos: Vector2
var tween_speed: float = 0.15


func _ready() -> void:
	button_pos = ($Header/ButtonBack as Button).rect_position
	REGEX.compile("(\\d)(?=(\\d\\d\\d)+([^\\d]|$))")


func _process(_delta) -> void:
	($Header/Score as Label).text = str(_format_number(round(score)))


func show_back(state: bool) -> void:
	var _t: bool
	if state:
		_t = ($Tween as Tween).interpolate_property(($Header/ButtonBack as Button), "rect_position", null, button_pos, tween_speed)
	else:
		_t = ($Tween as Tween).interpolate_property(($Header/ButtonBack as Button), "rect_position", null, button_pos - Vector2(100.0, 0.0), tween_speed)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


func set_score(count: int) -> void:
	var _t: bool = ($Tween as Tween).interpolate_property(self, "score", null, score + count * score_step, tween_speed * count)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# форматирование числа, проставление знаков между порядками
func _format_number(number: float) -> String:
	var formated_number: String = REGEX.sub(String(number), "$1,", true)
	return formated_number


func _on_ButtonBack_pressed() -> void:
	emit_signal("button_back_pressed")
