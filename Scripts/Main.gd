extends Node


enum ACTIVE_VIEW {
	MENU,
	GAME,
}


# BUILTINS - - - - - - - - -


func _ready() -> void:
	randomize()
	var _c: int = ($Menu as Control).connect("button_pressed", self, "_on_Menu_button_pressed")
	_c = ($GUI as Control).connect("button_back_pressed", self, "_on_GUI_button_back_pressed")
	_c = ($Game as Control).connect("icons_deleted", self, "_on_Game_icons_deleted")
	_show_hide_views(ACTIVE_VIEW.MENU)


# METHODS - - - - - - - - -


func _show_hide_views(state: int) -> void:
	if state == ACTIVE_VIEW.MENU:
		($Menu as Control).show()
		($Game as Control).hide()
		($GUI as Control).call("show_back", false)
	elif state == ACTIVE_VIEW.GAME:
		($Menu as Control).hide()
		($Game as Control).show()
		($GUI as Control).call("show_back", true)


# SIGNALS - - - - - - - - -


func _on_Menu_button_pressed(level: int) -> void:
	($Game as Control).call_deferred("load_level", level)
	_show_hide_views(ACTIVE_VIEW.GAME)


func _on_GUI_button_back_pressed() -> void:
	_show_hide_views(ACTIVE_VIEW.MENU)


func _on_Game_icons_deleted(count: int) -> void:
	($GUI as Control).call_deferred("set_score", count)
