extends TextureRect


# уровень
var level: int
# вид иконки
var type: int
# координаты в паттерне
var active: bool = false setget set_active, is_active # selected
var neighbour: bool = false setget set_neighbour, is_neighbour # selected like neighbour
# скорость твина
var speed: float = 0.15


# BUILTINS - - - - - - - - -


func _ready() -> void:
	set_icon_texture()


# METHODS - - - - - - - - -


# setter for active
func set_active(state: bool) -> void:
	if state:
		($AnimationPlayer as AnimationPlayer).play("selected")
	else:
		($AnimationPlayer as AnimationPlayer).stop()
		self.rect_scale = Vector2.ONE
	active = state


# getter for active
func is_active() -> bool:
	return active


# setter for neighbour
func set_neighbour(state: bool) -> void:
	if state:
		($AnimationPlayer as AnimationPlayer).play("neighboured")
	else:
		($AnimationPlayer as AnimationPlayer).stop()
		self.self_modulate.a = 1.0
	neighbour = state


# getter for neighbour
func is_neighbour() -> bool:
	return neighbour


# переместить на другую позицию на доске
func go_to(new_pos: Vector2) -> void:
	var _t: bool = ($Tween as Tween).interpolate_property(self, "rect_position", null, new_pos, speed)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


func set_icon_texture() -> void:
	self.texture = load("res://Resources/Sprites/ic_lvl_%s/%s.png" % [level, type])
	self.rect_size = Vector2.ZERO
	self.rect_pivot_offset = self.rect_size / 2.0


# удаление иконки с доски
func death(new_type: int, delay: float) -> void:
	type = new_type
	yield(get_tree().create_timer(delay), "timeout")
	var _t: bool = ($Tween as Tween).interpolate_property(self, "rect_scale", null, Vector2.ZERO, speed)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()
	yield(get_tree().create_timer(speed * 3.0), "timeout")
	set_icon_texture()
	_t = ($Tween as Tween).interpolate_property(self, "rect_scale", null, Vector2.ONE, speed)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()



# SIGNALS - - - - - - - - -
