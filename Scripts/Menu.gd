extends Control


signal button_pressed


func _on_BtnLvl1_pressed() -> void:
	emit_signal("button_pressed", 1)


func _on_BtnLvl2_pressed() -> void:
	emit_signal("button_pressed", 2)


func _on_BtnLvl3_pressed() -> void:
	emit_signal("button_pressed", 3)


func _on_BtnLvl4_pressed() -> void:
	emit_signal("button_pressed", 4)
